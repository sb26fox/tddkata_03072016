using System;
using System.Collections.Generic;

namespace Domain
{
    public class StringCalculator
    {
        private readonly int defaultValue = 0;
        private int sum;
        private Infrasrtucture.ILogger logger;
        private Infrasrtucture.IWebService webService;

        public StringCalculator(Infrasrtucture.ILogger logger,Infrasrtucture.IWebService webService)
        {
            this.logger = logger;
            this.webService = webService;
        }

        public int Add(string input)
        {
            if (IsEmpty(input) || HasWrongDelimiter(input))
            {
                return defaultValue;
            }

            if (HasCustomDelimiter(input))
            {
                string destNumbers;
                var delimiter = GetDelimiter(input, out destNumbers);

                var numbers = destNumbers.Split(delimiter, StringSplitOptions.None);

                sum = Count(numbers);

                WriteResult(input, sum);

                return sum;
            }

            if (HasDefaultDelimiter(input))
            {
                var numbers = input.Split(',', '\n');
                sum = Count(numbers);

                WriteResult(input, sum);

                return sum;
            }


            sum = ParseSingleNumber(input);

            WriteResult(input, sum);

            return sum;
        }

        private void WriteResult(string input, int sum)
        {
            try
            {
                logger.Write(input, sum);
            }
            catch (Exception ex)
            {
                webService.Notify(ex.Message);
            }
        }

        private string[] GetDelimiter(string input, out string destNumbers)
        {
            var delimiter = new List<string>();
            var index = input.IndexOf("\n");
            destNumbers = input.Substring(index);


            var subString = input.Remove(index);
            var tmp = subString.Split('[', ']');

            for (var i = 1; i < tmp.Length; i++)
            {
                delimiter.Add(tmp[i]);
            }
            return delimiter.ToArray();
        }

        private int Count(string[] numbers)
        {
            var sum = 0;
            var message = string.Empty;

            foreach (var number in numbers)
            {
                if (ParseSingleNumber(number) < 0)
                {
                    message = string.Concat(message, " ", number);
                }

                if (!IsEmpty(message))
                {
                    throw new NegativeNumberException("Input has negative numbers " + message);
                }

                if (ParseSingleNumber(number) < 1000)
                {
                    sum += ParseSingleNumber(number);
                }
            }

            return sum;
        }

        private bool HasCustomDelimiter(string input)
        {
            return input.StartsWith("//");
        }

        private bool HasWrongDelimiter(string input)
        {
            return input.Contains(",\n");
        }

        private bool HasDefaultDelimiter(string input)
        {
            return input.Contains(",") || input.Contains("\n");
        }

        private bool IsEmpty(string input)
        {
            return string.IsNullOrEmpty(input);
        }

        private int ParseSingleNumber(string input)
        {
            return int.Parse(input);
        }
    }
}