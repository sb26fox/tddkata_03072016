﻿using System.IO;
using Domain;
using Infrasrtucture;
using Moq;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class StringCalculatorShould
    {
        private StringCalculator calculator;
        private Mock<ILogger> defaultLogger = new Mock<ILogger>();
        private Mock<IWebService> defaultWebService = new Mock<IWebService>();

        [SetUp]
        public void SetUp()
        {
            defaultLogger.Setup(x => x.Write(It.IsAny<string>(), It.IsAny<int>())).Returns(true);
            defaultWebService.Setup(x => x.Notify(It.IsAny<string>())).Returns(true);

            calculator = new StringCalculator(defaultLogger.Object, defaultWebService.Object);
        }

        [Test]
        public void IgnoreNumbersMoreThan1000IfInputHasNumberThan1000()
        {
            var sum = calculator.Add("2,1001");

            Assert.AreEqual(2, sum);
        }

        [Test]
        public void NotifyWebServiceIfLoggerThrowsException()
        {
            defaultLogger.Setup(x => x.Write("1,2", 3)).Throws<FileNotFoundException>();
            var calculator = new StringCalculator(defaultLogger.Object,defaultWebService.Object);

            calculator.Add("1,2");

            defaultWebService.Verify(x => x.Notify(It.IsAny<string>()));
        }

        [Test]
        public void ReturmSumOfNumbersIfInputHasCustomDelimiter()
        {
            var sum = calculator.Add("//[;]\n1;2");

            Assert.AreEqual(1 + 2, sum);
        }

        [Test]
        public void Return1IfInputIs1()
        {
            var sum = calculator.Add("1");

            Assert.AreEqual(1, sum);
        }

        [Test]
        public void ReturnAnyNumberIfInputIsAnyNunmber()
        {
            var sum = calculator.Add("2");

            Assert.AreEqual(2, sum);
        }

        [Test]
        public void ReturnSumOfAnyTwoNumbersIfInputHasTwoNumbers()
        {
            var sum = calculator.Add("2,3");

            Assert.AreEqual(2 + 3, sum);
        }

        [Test]
        public void ReturnSumOfNumbersIfInputHasAnyLengthDelimiter()
        {
            double sum = calculator.Add("//[**]\n1**2**3");

            Assert.AreEqual(1 + 2 + 3, sum);
        }

        [Test]
        public void ReturnSumOfNumbersIfInputHasMultipleDelimiters()
        {
            var sum = calculator.Add("//[*][%]\n1*2%3");

            Assert.AreEqual(1 + 2 + 3, sum);
        }

        [Test]
        public void ReturnSumOfNumbersIfInputHasMultipleDelimitersLongerThanOneChar()
        {
            var sum = calculator.Add("//[**][$%]\n1**2$%3");

            Assert.AreEqual(1 + 2 + 3, sum);
        }

        [Test]
        public void ReturnSumOfNumbersIfInputHasNewLineDelimiter()
        {
            var sum = calculator.Add("1\n2,3");

            Assert.AreEqual(1 + 2 + 3, sum);
        }

        [Test]
        public void ReturnSumOfSeveralNumbersIfInputHasSeveralNumber()
        {
            var sum = calculator.Add("1,2,3");

            Assert.AreEqual(1 + 2 + 3, sum);
        }

        [Test]
        public void ReturnSumOfTwoNumbersIfInputHasTwoNumbers()
        {
            var sum = calculator.Add("1,2");

            Assert.AreEqual(1 + 2, sum);
        }

        [Test]
        public void ReturnZeroIfInputHasWrongDelimiter()
        {
            var sum = calculator.Add("1,\n2,3");

            Assert.AreEqual(0, sum);
        }

        [Test]
        public void ReturnZeroIfInputIsEmpty()
        {
            var sum = calculator.Add(string.Empty);

            Assert.AreEqual(0, sum);
        }

        [Test]
        public void ThrowsNegativeNumberExceptionIfInputHasNegativeNumbers()
        {
            Assert.Throws<NegativeNumberException>(() => calculator.Add("-1,2"));
        }

        [Test]
        public void WriteResultToLogger()
        {
            var sum = calculator.Add("1,2");

            defaultLogger.Verify(x => x.Write("1,2", 3));
        }
    }
}